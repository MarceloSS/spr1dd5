import {Component} from 'react';
import Character from './character';
import './App.css';

class App extends Component{
  state = { characterList: [] };

  componentDidMount(){
    fetch("https://rickandmortyapi.com/api/character/")
      .then((res) => res.json())
      .then((res) => {this.setState({ characterList: res.results})
    });
  }

  render(){
    const { characterList } = this.state
    return (
      <div className="App">
        <header className="App-header">
          <Character list={characterList} />
        </header>
      </div>
    );
  }
}

export default App;
