const Character = ({list}) => {

    console.log(list)
    return(
        list.map((character, index) => (
            <div key={ index } style={{width:'400px', border:'2px solid white', padding:'10px'}}>
                <img src={ character.image } alt={character.name} ></img>
                <h1>{ character.name }</h1>
                <div style={{display:'flex', flexDirection:'row', justifyContent:'space-between'}}>
                    <h2>{ character.species }</h2>
                    <h2>{ character.gender }</h2>
                </div>
                <div style={{display:'flex', flexDirection:'row', justifyContent:'space-between'}}>
                    <h2>{ character.origin.name }</h2>
                    <h2>{ character.status }</h2>
                </div>
            </div>
        ))
    )
}
export default Character